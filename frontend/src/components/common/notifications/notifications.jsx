import * as React from 'react';
import PropTypes from 'prop-types';
import { Stomp } from '@stomp/stompjs';
import SockJS from 'sockjs-client';
import {
  NotificationContainer,
  NotificationManager
} from 'react-notifications';
import { userType } from 'src/common/prop-types/prop-types';

import 'react-notifications/lib/notifications.css';

const Notifications = ({ user, onPostApply }) => {
  const [stompClient] = React.useState(Stomp.over(new SockJS('/ws')));
  React.useEffect(() => {
    if (!user) {
      return undefined;
    }

    stompClient.debug = () => { };
    stompClient.connect({}, () => {
      const { id } = user;

      stompClient.subscribe(`/user/${id}/like`, () => {
        NotificationManager.info('Your post was liked!');
      });

      stompClient.subscribe(`/user/${id}/postAdded`, () => {
        NotificationManager.success('Your post was created!');
      });

      stompClient.subscribe(`/user/${id}/postDeleted`, () => {
        NotificationManager.success('Your post has been deleted.');
      });

      stompClient.subscribe(`/user/${id}/postEdited`, () => {
        NotificationManager.success('Your post has been edited.');
      });

      stompClient.subscribe(`/user/${id}/commentAdded`, () => {
        NotificationManager.success('Your comment was created!');
      });

      stompClient.subscribe(`/user/${id}/commentDeleted`, () => {
        NotificationManager.success('Your comment has been deleted.');
      });

      stompClient.subscribe(`/user/${id}/commentEdited`, () => {
        NotificationManager.success('Your comment has been edited.');
      });

      stompClient.subscribe(`/user/${id}/avatarChanged`, () => {
        NotificationManager.success('Your avatar has been changed.');
      });

      stompClient.subscribe(`/user/${id}/statusChanged`, () => {
        NotificationManager.success('Your status has been changed.');
      });

      stompClient.subscribe(`/user/${id}/emailChanged`, () => {
        NotificationManager.success('Your email has been changed.');
      });

      stompClient.subscribe('/topic/new_post', message => {
        const post = JSON.parse(message.body);
        if (post.userId !== id) {
          onPostApply(post.id);
        }
      });
    });

    return () => {
      stompClient.onDisconnect(() => {});
    };
  });

  return <NotificationContainer />;
};

Notifications.defaultProps = {
  user: undefined
};

Notifications.propTypes = {
  user: userType,
  onPostApply: PropTypes.func.isRequired
};

export default Notifications;
