import * as React from 'react';
import PropTypes from 'prop-types';
import { ButtonType } from 'src/common/enums/enums';
import { Button, Form } from 'src/components/common/common';
import { getCommentOnEdit, setCommentOnEdit } from 'src/components/thread/components/expanded-post/expanded-post';

const AddComment = ({ postId, onCommentAdd, onCommentEdit }) => {
  const [body, setBody] = React.useState('');

  const handleEditComment = async comment => {
    onCommentEdit({ id: comment.id, body });
    setBody('');
    setCommentOnEdit(undefined);
    document.getElementsByClassName('editCommentMarker')[0].style.display = 'none';
  };

  const handleAddComment = async () => {
    if (!body) {
      setCommentOnEdit(undefined);
      return;
    }
    const commentOnEdit = getCommentOnEdit();
    if (commentOnEdit !== undefined) {
      await handleEditComment(commentOnEdit);
      return;
    }
    await onCommentAdd({ postId, body });
    setBody('');
  };

  return (
    <Form reply onSubmit={handleAddComment}>
      <span className="editCommentMarker">
        You are editing your comment.
      </span>
      <Form.TextArea
        value={body}
        placeholder="Type a comment..."
        onChange={ev => setBody(ev.target.value)}
      />
      <Button type={ButtonType.SUBMIT} isPrimary>
        Post comment
      </Button>
    </Form>
  );
};

AddComment.propTypes = {
  onCommentAdd: PropTypes.func.isRequired,
  onCommentEdit: PropTypes.func.isRequired,
  postId: PropTypes.string.isRequired
};

export default AddComment;
