import * as React from 'react';
import { getFromNowTime } from 'src/helpers/helpers';
import { DEFAULT_USER_AVATAR } from 'src/common/constants/constants';
import { Card, Comment as CommentUI, Icon, Label } from 'src/components/common/common';
import { commentType } from 'src/common/prop-types/prop-types';
import PropTypes from 'prop-types';
import { IconName } from 'src/common/enums/enums';

import styles from './styles.module.scss';

const Comment = ({ comment: { id, body, createdAt, user, likeCount, dislikeCount }, currentUserId, onCommentLike,
  onCommentDislike, onCommentDelete, onCommentEdit }) => {
  const handleCommentLike = () => onCommentLike(id);
  const handleCommentDislike = () => onCommentDislike(id);
  const handleCommentDelete = () => onCommentDelete(id);
  const handleCommentEdit = () => onCommentEdit({ id, body });

  return (
    <CommentUI className={styles.comment}>
      <CommentUI.Avatar src={user.image?.link ?? DEFAULT_USER_AVATAR} />
      <CommentUI.Content>
        <CommentUI.Metadata className={styles.meta}>
          <CommentUI.Author as="a">{user.username}</CommentUI.Author>
          {getFromNowTime(createdAt)}
          {currentUserId === user.id && (
            <Label
              basic
              size="small"
              as="a"
              className={styles.deleteComment}
              onClick={handleCommentDelete}
            >
              <Icon name={IconName.CLOSE} />
            </Label>
          )}
          {currentUserId === user.id && (
            <Label
              basic
              size="small"
              as="a"
              className={styles.editComment}
              onClick={handleCommentEdit}
            >
              <Icon name={IconName.EDIT} />
            </Label>
          )}
        </CommentUI.Metadata>
        <CommentUI.Metadata className={styles.meta}>
          <span className="status">
            {user.status}
          </span>
        </CommentUI.Metadata>
        <CommentUI.Text>{body}</CommentUI.Text>
      </CommentUI.Content>
      <Card.Content extra>
        <Label
          basic
          size="small"
          as="a"
          className={styles.toolbarBtn}
          onClick={handleCommentLike}
        >
          <Icon name={IconName.THUMBS_UP} />
          {likeCount}
        </Label>
        <Label
          basic
          size="small"
          as="a"
          className={styles.toolbarBtn}
          onClick={handleCommentDislike}
        >
          <Icon name={IconName.THUMBS_DOWN} />
          {dislikeCount}
        </Label>
      </Card.Content>
    </CommentUI>
  );
};

Comment.propTypes = {
  comment: commentType.isRequired,
  currentUserId: PropTypes.string.isRequired,
  onCommentLike: PropTypes.func.isRequired,
  onCommentDislike: PropTypes.func.isRequired,
  onCommentEdit: PropTypes.func.isRequired,
  onCommentDelete: PropTypes.func.isRequired
};

export default Comment;
