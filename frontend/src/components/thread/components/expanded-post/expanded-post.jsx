import * as React from 'react';
import PropTypes from 'prop-types';
import { useDispatch, useSelector } from 'react-redux';
import { threadActionCreator } from 'src/store/actions';
import { Spinner, Post, Modal, Comment as CommentUI } from 'src/components/common/common';
import AddComment from '../add-comment/add-comment';
import Comment from '../comment/comment';
import { getSortedComments } from './helpers/helpers';

let commentOnEdit;
export const setCommentOnEdit = comment => {
  commentOnEdit = comment;
  if (comment !== undefined) {
    document.getElementsByTagName('textarea')[1].value = comment.body;
    window.scrollTo(0, document.getElementsByClassName('content')[0].scrollHeight);
    document.getElementsByClassName('editCommentMarker')[0].style.display = 'inline-block';
  } else {
    document.getElementsByTagName('textarea')[1].value = '';
    document.getElementsByClassName('editCommentMarker')[0].style.display = 'none';
  }
};
export const getCommentOnEdit = () => commentOnEdit;

const ExpandedPost = ({ sharePost }) => {
  const dispatch = useDispatch();
  const { post, userId } = useSelector(state => ({
    post: state.posts.expandedPost,
    userId: state.profile.user.id
  }));

  const handlePostLike = React.useCallback(id => (
    dispatch(threadActionCreator.likePost(id))
  ), [dispatch]);

  const handlePostDislike = React.useCallback(id => (
    dispatch(threadActionCreator.dislikePost(id))
  ), [dispatch]);

  const handleCommentAdd = React.useCallback(commentPayload => (
    dispatch(threadActionCreator.addComment(commentPayload))
  ), [dispatch]);

  const handleCommentLike = React.useCallback(id => (
    dispatch(threadActionCreator.likeComment(id))
  ), [dispatch]);

  const handleCommentDislike = React.useCallback(id => (
    dispatch(threadActionCreator.dislikeComment(id))
  ), [dispatch]);

  const handleCommentDelete = React.useCallback(id => (
    dispatch(threadActionCreator.deleteComment(id))
  ), [dispatch]);

  const handleCommentEdit = React.useCallback(commentPayload => (
    dispatch(threadActionCreator.editComment(commentPayload))
  ), [dispatch]);

  const handleExpandedPostToggle = React.useCallback(id => (
    dispatch(threadActionCreator.toggleExpandedPost(id))
  ), [dispatch]);

  const handleExpandedPostClose = () => handleExpandedPostToggle();

  const sortedComments = getSortedComments(post.comments ?? []);

  return (
    <Modal
      dimmer="blurring"
      centered={false}
      open
      onClose={handleExpandedPostClose}
    >
      {post ? (
        <Modal.Content>
          <Post
            post={post}
            onPostLike={handlePostLike}
            onPostDislike={handlePostDislike}
            onExpandedPostToggle={handleExpandedPostToggle}
            sharePost={sharePost}
            onPostEdit={() => {}}
            onPostDelete={() => {}}
            userId="0"
          />
          <CommentUI.Group style={{ maxWidth: '100%' }}>
            <h3>Comments</h3>
            {sortedComments.map(comment => (
              <Comment
                key={comment.id}
                comment={comment}
                currentUserId={userId}
                onCommentLike={handleCommentLike}
                onCommentDislike={handleCommentDislike}
                onCommentDelete={handleCommentDelete}
                onCommentEdit={setCommentOnEdit}
              />
            ))}
            <AddComment postId={post.id} onCommentAdd={handleCommentAdd} onCommentEdit={handleCommentEdit} />
          </CommentUI.Group>
        </Modal.Content>
      ) : (
        <Spinner />
      )}
    </Modal>
  );
};

ExpandedPost.propTypes = {
  sharePost: PropTypes.func.isRequired
};

export default ExpandedPost;
