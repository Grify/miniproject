import * as React from 'react';
import PropTypes from 'prop-types';
import { ButtonColor, ButtonType, IconName } from 'src/common/enums/enums';
import { Button, Form, Image, Label, Segment } from 'src/components/common/common';
import { getPostOnEdit, setPostOnEdit } from 'src/components/thread/thread';

import styles from './styles.module.scss';

const AddPost = ({ onPostAdd, uploadImage, onPostEdit }) => {
  const [body, setBody] = React.useState('');
  const [image, setImage] = React.useState(undefined);
  const [isUploading, setIsUploading] = React.useState(false);
  const [loadingError, setLoadingError] = React.useState('');

  const handleEditPost = async post => {
    onPostEdit({ postId: post.id, imageId: image?.imageId, body });
    setBody('');
    setImage(undefined);
    setPostOnEdit(undefined);
    document.getElementsByClassName('editPostMarker')[0].style.display = 'none';
  };

  const handleAddPost = async () => {
    if (!body) {
      setPostOnEdit(undefined);
      return;
    }
    const post = getPostOnEdit();
    if (post !== undefined) {
      await handleEditPost(post);
      return;
    }
    await onPostAdd({ imageId: image?.imageId, body });
    setBody('');
    setImage(undefined);
  };

  const handleUploadFile = ({ target }) => {
    setIsUploading(true);
    const [file] = target.files;

    uploadImage(file)
      .then(({ id: imageId, link: imageLink }) => {
        setImage({ imageId, imageLink });
        setLoadingError('');
      })
      .catch(() => {
        setLoadingError('Error while loading. Please try again.');
      })
      .finally(() => {
        setIsUploading(false);
      });
  };

  return (
    <Segment>
      <span className="editPostMarker">
        You are editing your post.
      </span>
      <Form onSubmit={handleAddPost}>
        <Form.TextArea
          name="body"
          value={body}
          placeholder="What is the news?"
          onChange={ev => setBody(ev.target.value)}
        />
        {loadingError && (
          <Label basic color="red" pointing="below">
            {loadingError}
          </Label>
        )}
        {image?.imageLink && (
          <div className={styles.imageWrapper}>
            <Image className={styles.image} src={image?.imageLink} alt="post" />
          </div>
        )}
        <div className={styles.btnWrapper}>
          <Button
            color="teal"
            isLoading={isUploading}
            isDisabled={isUploading}
            iconName={IconName.IMAGE}
          >
            <label className={styles.btnImgLabel}>
              Attach image
              <input
                name="image"
                type="file"
                onChange={handleUploadFile}
                hidden
              />
            </label>
          </Button>
          <Button color={ButtonColor.BLUE} type={ButtonType.SUBMIT}>
            Post
          </Button>
        </div>
      </Form>
    </Segment>
  );
};

AddPost.propTypes = {
  onPostAdd: PropTypes.func.isRequired,
  uploadImage: PropTypes.func.isRequired,
  onPostEdit: PropTypes.func.isRequired
};

export default AddPost;
