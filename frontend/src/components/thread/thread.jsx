import * as React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import InfiniteScroll from 'react-infinite-scroller';
import { threadActionCreator } from 'src/store/actions';
import { image as imageService } from 'src/services/services';
import { Post, Spinner, Checkbox } from 'src/components/common/common';
import { ExpandedPost, SharedPostLink, AddPost } from './components/components';

import styles from './styles.module.scss';

const postsFilter = {
  userId: undefined,
  from: 0,
  count: 10,
  onlyOwnPosts: false,
  onlyLiked: false,
  onlyOthers: false
};

let postOnEdit;

export const setPostOnEdit = post => {
  postOnEdit = post;
  if (post !== undefined) {
    document.getElementsByTagName('textarea')[0].value = post.body;
    window.scrollTo({ top: 0, behavior: 'smooth' });
    document.getElementsByClassName('editPostMarker')[0].style.display = 'inline-block';
  } else {
    document.getElementsByTagName('textarea')[0].value = '';
    document.getElementsByClassName('editPostMarker')[0].style.display = 'none';
  }
};

export const getPostOnEdit = () => postOnEdit;

const Thread = () => {
  const { posts, hasMorePosts, expandedPost, userId } = useSelector(state => ({
    posts: state.posts.posts,
    hasMorePosts: state.posts.hasMorePosts,
    expandedPost: state.posts.expandedPost,
    userId: state.profile.user.id
  }));
  const [sharedPostId, setSharedPostId] = React.useState(undefined);
  const [showOwnPosts, setShowOwnPosts] = React.useState(false);
  const [showOthersPosts, setShowOthersPosts] = React.useState(false);
  const [showLikedPosts, setShowLikedPosts] = React.useState(false);

  const dispatch = useDispatch();

  const handlePostLike = React.useCallback(id => (
    dispatch(threadActionCreator.likePost(id))
  ), [dispatch]);

  const handlePostDislike = React.useCallback(id => (
    dispatch(threadActionCreator.dislikePost(id))
  ), [dispatch]);

  const handlePostEdit = React.useCallback(postPayload => (
    dispatch(threadActionCreator.editPost(postPayload))
  ), [dispatch]);

  const handlePostDelete = React.useCallback(id => (
    dispatch(threadActionCreator.deletePost(id))
  ), [dispatch]);

  const handleExpandedPostToggle = React.useCallback(id => (
    dispatch(threadActionCreator.toggleExpandedPost(id))
  ), [dispatch]);

  const handlePostAdd = React.useCallback(postPayload => (
    dispatch(threadActionCreator.createPost(postPayload))
  ), [dispatch]);

  const handlePostsLoad = filtersPayload => {
    dispatch(threadActionCreator.loadPosts(filtersPayload));
  };

  const handleMorePostsLoad = filtersPayload => {
    dispatch(threadActionCreator.loadMorePosts(filtersPayload));
  };

  const toggleShowOwnPosts = () => {
    setShowOwnPosts(!showOwnPosts);
    setShowLikedPosts(false);
    setShowOthersPosts(false);
    postsFilter.userId = !showOwnPosts ? userId : undefined;
    postsFilter.onlyOwnPosts = !showOwnPosts;
    postsFilter.onlyLiked = false;
    postsFilter.onlyOthers = false;
    postsFilter.from = 0;
    handlePostsLoad(postsFilter);
    postsFilter.from = postsFilter.count; // for the next scroll
  };

  const toggleShowOthersPosts = () => {
    setShowOthersPosts(!showOthersPosts);
    setShowOwnPosts(false);
    setShowLikedPosts(false);
    postsFilter.userId = !showOthersPosts ? userId : undefined;
    postsFilter.onlyOthers = !showOthersPosts;
    postsFilter.onlyLiked = false;
    postsFilter.onlyOwnPosts = false;
    postsFilter.from = 0;
    handlePostsLoad(postsFilter);
    postsFilter.from = postsFilter.count; // for the next scroll
  };

  const toggleShowLikedPosts = () => {
    setShowLikedPosts(!showLikedPosts);
    setShowOwnPosts(false);
    setShowOthersPosts(false);
    postsFilter.userId = !showLikedPosts ? userId : undefined;
    postsFilter.onlyLiked = !showLikedPosts;
    postsFilter.onlyOwnPosts = false;
    postsFilter.onlyOthers = false;
    postsFilter.from = 0;
    handlePostsLoad(postsFilter);
    postsFilter.from = postsFilter.count; // for the next scroll
  };

  const getMorePosts = () => {
    handleMorePostsLoad(postsFilter);
    const { from, count } = postsFilter;
    postsFilter.from = from + count;
  };

  const sharePost = id => setSharedPostId(id);

  const uploadImage = file => imageService.uploadImage(file);

  return (
    <div className={styles.threadContent}>
      <div className={styles.addPostForm}>
        <AddPost
          onPostAdd={handlePostAdd}
          uploadImage={uploadImage}
          onPostEdit={handlePostEdit}
        />
      </div>
      <div className={styles.toolbar}>
        <Checkbox
          toggle
          className={styles.checkBox}
          label="Show only my posts"
          checked={showOwnPosts}
          onChange={toggleShowOwnPosts}
        />
        <Checkbox
          toggle
          className={styles.checkBox}
          label="Show only another's posts"
          checked={showOthersPosts}
          onChange={toggleShowOthersPosts}
        />
        <Checkbox
          toggle
          className={styles.checkBox}
          label="Show only liked posts"
          checked={showLikedPosts}
          onChange={toggleShowLikedPosts}
        />
      </div>
      <InfiniteScroll
        pageStart={0}
        loadMore={getMorePosts}
        hasMore={hasMorePosts}
        loader={<Spinner key="0" />}
      >
        {posts.map(post => (
          <Post
            post={post}
            onPostLike={handlePostLike}
            onPostDislike={handlePostDislike}
            onPostEdit={setPostOnEdit}
            onPostDelete={handlePostDelete}
            onExpandedPostToggle={handleExpandedPostToggle}
            sharePost={sharePost}
            userId={userId}
            key={post.id}
          />
        ))}
      </InfiniteScroll>
      {expandedPost && (
        <ExpandedPost
          sharePost={sharePost}
          currentUserId={userId}
        />
      )}
      {sharedPostId && (
        <SharedPostLink
          postId={sharedPostId}
          close={() => setSharedPostId(undefined)}
        />
      )}
    </div>
  );
};

export default Thread;
