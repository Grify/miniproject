import * as React from 'react';
import { useSelector } from 'react-redux';
import { Grid, Image, Input, Form, Button, Label } from 'src/components/common/common';
import { DEFAULT_USER_AVATAR } from 'src/common/constants/constants';
import { ButtonColor, ButtonType, IconName } from 'src/common/enums/enums';
import { image as imageService, auth as authService } from 'src/services/services';

import styles from './styles.module.scss';

const Profile = () => {
  const { user } = useSelector(state => ({
    user: state.profile.user
  }));
  const [emailError, setEmailError] = React.useState('');
  const [statusError, setStatusError] = React.useState('');
  const [email, setEmail] = React.useState('');
  const [status, setStatus] = React.useState('');
  const [image, setImage] = React.useState(undefined);
  const [isUploading, setIsUploading] = React.useState(false);
  const [loadingError, setLoadingError] = React.useState('');

  const onStatusUpload = () => authService.uploadStatus(status);
  const setImageUpload = () => authService.setImage(image?.imageId);
  const onEmailUpload = () => authService.uploadEmail(email);

  const handleUploadStatus = async () => {
    if (!status) {
      return;
    }
    if (status.length > 70) {
      setStatusError('Status is too long, max length is 70 chars.');
    } else {
      setStatusError('');
      const response = await onStatusUpload();
      if (response.response === 'Success.') {
        // TODO
      } else {
        setStatusError(response.response);
      }
      setStatus('');
    }
  };

  const handleUploadEmail = async () => {
    if (!email) {
      setEmailError('Email is empty.');
      return;
    }
    if (!/^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/.test(email)) {
      setEmailError('Your email is invalid.');
      return;
    }
    const response = await onEmailUpload();
    if (response.response === 'Success.') {
      // TODO
    } else {
      setEmailError(response.response);
    }
    setEmail('');
  };

  const uploadImage = file => imageService.uploadImage(file);

  const handleUploadFile = ({ target }) => {
    setIsUploading(true);
    const [file] = target.files;

    uploadImage(file)
      .then(({ id: imageId, link: imageLink }) => {
        setImage({ imageId, imageLink });
      })
      .catch(() => {
        setLoadingError('Error while loading. Please try again.');
      })
      .finally(() => {
        setIsUploading(false);
      });
  };

  return (
    <Grid container textAlign="center" style={{ paddingTop: 30 }}>
      <Grid.Row>
        <Grid.Column width={8}>
          <Image
            centered
            src={user.image?.link ?? DEFAULT_USER_AVATAR}
            size="medium"
            circular
          />
        </Grid.Column>
        <Grid.Column width={8}>
          <Input
            icon="user"
            iconPosition="left"
            placeholder="Username"
            disabled
            type="text"
            value={user.username}
          />
          <br />
          <br />
          <Input
            icon="at"
            iconPosition="left"
            placeholder="Email"
            type="email"
            disabled
            value={user.email}
          />
          <br />
          <br />
          <Input
            icon="at"
            iconPosition="left"
            placeholder="Status"
            type="text"
            disabled
            value={user.status}
          />
        </Grid.Column>
      </Grid.Row>
      <Grid.Row>
        <Grid.Column width={4}>
          <h3>Change your picture</h3>
          <Form onSubmit={setImageUpload}>
            {loadingError && (
              <Label basic color="red" pointing="below">
                {loadingError}
              </Label>
            )}
            {image?.imageLink && (
              <div className={styles.imageWrapper}>
                <Image className={styles.image} src={image?.imageLink} alt="post" />
              </div>
            )}
            <div className={styles.btnWrapper}>
              <Button
                color="teal"
                isLoading={isUploading}
                isDisabled={isUploading}
                iconName={IconName.IMAGE}
              >
                <label className={styles.btnImgLabel}>
                  Attach image
                  <input
                    name="image"
                    type="file"
                    onChange={handleUploadFile}
                    hidden
                  />
                </label>
              </Button>
              <Button color={ButtonColor.BLUE} type={ButtonType.SUBMIT}>
                Upload
              </Button>
            </div>
          </Form>
        </Grid.Column>
        <Grid.Column width={4}>
          <h3>Change your status</h3>
          <Form onSubmit={handleUploadStatus}>
            {statusError && (
              <Label basic color="red" pointing="below">
                {statusError}
              </Label>
            )}
            <Form.TextArea
              name="status"
              value={status}
              placeholder="Your status"
              onChange={ev => setStatus(ev.target.value)}
            />
            <Button color={ButtonColor.BLUE} type={ButtonType.SUBMIT}>
              Edit
            </Button>
          </Form>
        </Grid.Column>
        <Grid.Column width={4}>
          <h3>Change your email</h3>
          <Form onSubmit={handleUploadEmail}>
            {emailError && (
              <Label basic color="red" pointing="below">
                {emailError}
              </Label>
            )}
            <Input
              icon="at"
              iconPosition="left"
              placeholder="Email"
              type="email"
              name="email"
              value={email}
              onChange={ev => setEmail(ev.target.value)}
              className={styles.inputEmail}
            />
            <br />
            <Button color={ButtonColor.BLUE} type={ButtonType.SUBMIT}>
              Edit
            </Button>
          </Form>
        </Grid.Column>
      </Grid.Row>
    </Grid>
  );
};

export default Profile;
