import { createAction } from '@reduxjs/toolkit';
import {
  comment as commentService,
  post as postService
} from 'src/services/services';

const ActionType = {
  ADD_POST: 'thread/add-post',
  LOAD_MORE_POSTS: 'thread/load-more-posts',
  SET_ALL_POSTS: 'thread/set-all-posts',
  SET_EXPANDED_POST: 'thread/set-expanded-post',
  SET_ALL_COMMENTS: 'thread/set-all-comments'
};

const setPosts = createAction(ActionType.SET_ALL_POSTS, posts => ({
  payload: {
    posts
  }
}));

const addMorePosts = createAction(ActionType.LOAD_MORE_POSTS, posts => ({
  payload: {
    posts
  }
}));

const addPost = createAction(ActionType.ADD_POST, post => ({
  payload: {
    post
  }
}));

const setExpandedPost = createAction(ActionType.SET_EXPANDED_POST, post => ({
  payload: {
    post
  }
}));

const loadPosts = filter => async dispatch => {
  const posts = await postService.getAllPosts(filter);
  dispatch(setPosts(posts));
};

const loadMorePosts = filter => async (dispatch, getRootState) => {
  const {
    posts: { posts }
  } = getRootState();
  const loadedPosts = await postService.getAllPosts(filter);
  const filteredPosts = loadedPosts.filter(
    post => !(posts && posts.some(loadedPost => post.id === loadedPost.id))
  );
  dispatch(addMorePosts(filteredPosts));
};

const applyPost = postId => async dispatch => {
  const post = await postService.getPost(postId);
  dispatch(addPost(post));
};

const createPost = post => async dispatch => {
  const { id } = await postService.addPost(post);
  const newPost = await postService.getPost(id);
  dispatch(addPost(newPost));
};

const editPost = post => async (dispatch, getRootState) => {
  await postService.editPost(post);
  const newPost = await postService.getPost(post.postId);
  const {
    posts: { posts }
  } = getRootState();
  const updated = posts.map(p => (p.id === post.postId ? newPost : p));
  dispatch(setPosts(updated));
};

const deletePost = postId => async (dispatch, getRootState) => {
  await postService.deletePost(postId);
  const {
    posts: { posts }
  } = getRootState();
  const updated = posts.filter(post => (post.id !== postId));
  dispatch(setPosts(updated));
};

const toggleExpandedPost = postId => async dispatch => {
  const post = postId ? await postService.getPost(postId) : undefined;

  dispatch(setExpandedPost(post));
};

const likePost = postId => async (dispatch, getRootState) => {
  const response = await postService.likePost(postId);
  const diff = response?.id ? 1 : -1; // if ID exists then the post was liked, otherwise - like was removed
  const wasChanged = response?.wasChanged ? -1 : 0;
  const mapLikes = post => ({
    ...post,
    likeCount: Number(post.likeCount) + diff, // diff is taken from the current closure
    dislikeCount: Number(post.dislikeCount) + wasChanged
  });

  const {
    posts: { posts, expandedPost }
  } = getRootState();
  const updated = posts.map(post => (post.id !== postId ? post : mapLikes(post)));

  dispatch(setPosts(updated));

  if (expandedPost && expandedPost.id === postId) {
    dispatch(setExpandedPost(mapLikes(expandedPost)));
  }
};

const dislikePost = postId => async (dispatch, getRootState) => {
  const response = await postService.dislikePost(postId);
  const diff = response?.id ? 1 : -1; // if ID exists then the post was disliked, otherwise - dislike was removed
  const wasChanged = response?.wasChanged ? -1 : 0;

  const mapDislikes = post => ({
    ...post,
    dislikeCount: Number(post.dislikeCount) + diff, // diff is taken from the current closure
    likeCount: Number(post.likeCount) + wasChanged
  });

  const {
    posts: { posts, expandedPost }
  } = getRootState();
  const updated = posts.map(post => (post.id !== postId ? post : mapDislikes(post)));

  dispatch(setPosts(updated));

  if (expandedPost && expandedPost.id === postId) {
    dispatch(setExpandedPost(mapDislikes(expandedPost)));
  }
};

const likeComment = commentId => async (dispatch, getRootState) => {
  const response = await commentService.likeComment(commentId);
  const newComment = await commentService.getComment(commentId);
  const diff = response?.id ? 1 : -1; // if ID exists then the post was liked, otherwise - like was removed
  const wasChanged = response?.wasChanged ? -1 : 0;

  const mapLikes = comment => ({
    ...comment,
    likeCount: Number(comment.likeCount) + diff, // diff is taken from the current closure
    dislikeCount: Number(comment.dislikeCount) + wasChanged
  });
  const mapComments = post => ({
    ...post,
    comments: [...(post.comments || []).map(item => (item.id === commentId ? mapLikes(item) : item))]
  });

  const {
    posts: { posts, expandedPost }
  } = getRootState();
  const updated = posts.map(post => (post.id !== newComment.postId ? post : mapComments(post)));

  dispatch(setPosts(updated));

  if (expandedPost && expandedPost.id === newComment.postId) {
    dispatch(setExpandedPost(mapComments(expandedPost)));
  }
};

const dislikeComment = commentId => async (dispatch, getRootState) => {
  const response = await commentService.dislikeComment(commentId);
  const newComment = await commentService.getComment(commentId);
  const diff = response?.id ? 1 : -1; // if ID exists then the post was disliked, otherwise - dislike was removed
  const wasChanged = response?.wasChanged ? -1 : 0;

  const mapDislikes = comment => ({
    ...comment,
    dislikeCount: Number(comment.dislikeCount) + diff, // diff is taken from the current closure
    likeCount: Number(comment.likeCount) + wasChanged
  });
  const mapComments = post => ({
    ...post,
    comments: [...(post.comments || []).map(item => (item.id === commentId ? mapDislikes(item) : item))]
  });

  const {
    posts: { posts, expandedPost }
  } = getRootState();
  const updated = posts.map(post => (post.id !== newComment.postId ? post : mapComments(post)));

  dispatch(setPosts(updated));

  if (expandedPost && expandedPost.id === newComment.postId) {
    dispatch(setExpandedPost(mapComments(expandedPost)));
  }
};

const editComment = comment => async (dispatch, getRootState) => {
  await commentService.editComment(comment);
  const newComment = await commentService.getComment(comment.id);
  console.log(newComment);
  const mapComments = post => ({
    ...post,
    comments: [...(post.comments || []).map(item => (item.id === comment.id ? newComment : item))]
  });

  const {
    posts: { posts, expandedPost }
  } = getRootState();
  const updated = posts.map(post => (post.id !== newComment.postId ? post : mapComments(post)));

  dispatch(setPosts(updated));

  if (expandedPost && expandedPost.id === newComment.postId) {
    dispatch(setExpandedPost(mapComments(expandedPost)));
  }
};

const deleteComment = commentId => async (dispatch, getRootState) => {
  const comment = await commentService.getComment(commentId);
  await commentService.deleteComment(commentId);

  const mapComments = post => ({
    ...post,
    commentCount: Number(post.commentCount) - 1,
    comments: [...(post.comments || []).filter(c => c.id !== commentId)]
  });

  const {
    posts: { posts, expandedPost }
  } = getRootState();
  const updated = posts.map(post => (post.id !== comment.postId ? post : mapComments(post)));

  dispatch(setPosts(updated));

  if (expandedPost && expandedPost.id === comment.postId) {
    dispatch(setExpandedPost(mapComments(expandedPost)));
  }
};

const addComment = request => async (dispatch, getRootState) => {
  const { id } = await commentService.addComment(request);
  const comment = await commentService.getComment(id);
  const mapComments = post => ({
    ...post,
    commentCount: Number(post.commentCount) + 1,
    comments: [...(post.comments || []), comment] // comment is taken from the current closure
  });

  const {
    posts: { posts, expandedPost }
  } = getRootState();
  const updated = posts.map(post => (post.id !== comment.postId ? post : mapComments(post)));

  dispatch(setPosts(updated));

  if (expandedPost && expandedPost.id === comment.postId) {
    dispatch(setExpandedPost(mapComments(expandedPost)));
  }
};

export {
  setPosts,
  addMorePosts,
  addPost,
  setExpandedPost,
  editPost,
  deletePost,
  loadPosts,
  loadMorePosts,
  applyPost,
  createPost,
  toggleExpandedPost,
  likePost,
  dislikePost,
  likeComment,
  dislikeComment,
  deleteComment,
  editComment,
  addComment
};
