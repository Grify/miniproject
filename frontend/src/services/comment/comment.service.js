import { HttpMethod, ContentType } from 'src/common/enums/enums';

class Comment {
  constructor({ http }) {
    this._http = http;
  }

  getComment(id) {
    return this._http.load(`/api/comments/${id}`, {
      method: HttpMethod.GET
    });
  }

  addComment(payload) {
    return this._http.load('/api/comments', {
      method: HttpMethod.POST,
      contentType: ContentType.JSON,
      payload: JSON.stringify(payload)
    });
  }

  deleteComment(commentId) {
    return this._http.load('/api/comments', {
      method: HttpMethod.DELETE,
      query: { commentId }
    });
  }

  editComment(payload) {
    return this._http.load('/api/comments', {
      method: HttpMethod.PUT,
      contentType: ContentType.JSON,
      payload: JSON.stringify(payload)
    });
  }

  likeComment(commentId) {
    return this._http.load('/api/commentreaction', {
      method: HttpMethod.PUT,
      contentType: ContentType.JSON,
      payload: JSON.stringify({
        commentId,
        isLike: true,
        isDislike: false
      })
    });
  }

  dislikeComment(commentId) {
    return this._http.load('/api/commentreaction', {
      method: HttpMethod.PUT,
      contentType: ContentType.JSON,
      payload: JSON.stringify({
        commentId,
        isLike: false,
        isDislike: true
      })
    });
  }
}

export { Comment };
