package com.threadjava.comment;

import com.threadjava.comment.dto.CommentDetailsDto;
import com.threadjava.comment.dto.CommentEditDto;
import com.threadjava.comment.dto.CommentSaveDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.web.bind.annotation.*;
import java.util.UUID;
import static com.threadjava.auth.TokenService.getUserId;

@RestController
@RequestMapping("/api/comments")
public class CommentController {
    @Autowired
    private CommentService commentService;
    @Autowired
    private SimpMessagingTemplate template;

    @GetMapping("/{id}")
    public CommentDetailsDto get(@PathVariable UUID id) {
        return commentService.getCommentById(id);
    }

    @PostMapping
    public CommentDetailsDto post(@RequestBody CommentSaveDto commentDto) {
        commentDto.setUserId(getUserId());
        template.convertAndSendToUser(
                getUserId().toString(),
                "/commentAdded",
                "Your comment was created!"
        );
        return commentService.create(commentDto);
    }

    @DeleteMapping()
    public UUID delete(@RequestParam UUID commentId) {
        commentService.deleteCommentById(commentId);
        template.convertAndSendToUser(
                getUserId().toString(),
                "/commentDeleted",
                "Your comment was deleted."
        );
        return commentId;
    }

    @PutMapping
    public CommentEditDto put(@RequestBody CommentEditDto commentDto) {
        commentService.editComment(commentDto);
        template.convertAndSendToUser(
                getUserId().toString(),
                "/commentEdited",
                "Your comment was edited."
        );
        return commentDto;
    }
}
