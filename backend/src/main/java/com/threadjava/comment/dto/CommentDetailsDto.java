package com.threadjava.comment.dto;

import com.threadjava.post.dto.PostUserDto;
import lombok.Data;

import java.util.Date;
import java.util.UUID;

@Data
public class CommentDetailsDto {
    private UUID id;
    private String body;
    private long likeCount;
    private long dislikeCount;
    private PostUserDto user;
    private UUID postId;
    private Date createdAt;
    private Date updatedAt;
}
