package com.threadjava.comment.dto;

import lombok.Data;

import java.util.UUID;

@Data
public class CommentEditDto {
    private UUID id;
    private String body;

}
