package com.threadjava.comment;

import com.threadjava.comment.dto.CommentDetailsQueryResult;
import com.threadjava.comment.model.Comment;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.UUID;

public interface CommentRepository extends JpaRepository<Comment, UUID> {

    @Query("SELECT new com.threadjava.comment.dto.CommentDetailsQueryResult(c.id, c.body, " +
            "(SELECT COALESCE(SUM(CASE WHEN cr.isLike = TRUE THEN 1 ELSE 0 END), 0) FROM c.reactions cr WHERE cr.comment = c), " +
            "(SELECT COALESCE(SUM(CASE WHEN cr.isLike = FALSE THEN 1 ELSE 0 END), 0) FROM c.reactions cr WHERE cr.comment = c), " +
            "c.createdAt, c.updatedAt, c.user, c.post.id) " +
            "FROM Comment c " +
            "WHERE c.post.id = :postId AND c.isDeleted = FALSE")
    List<CommentDetailsQueryResult> findAllByPostId(@Param("postId") UUID postId);

    @Query("SELECT new com.threadjava.comment.dto.CommentDetailsQueryResult(c.id, c.body, " +
            "(SELECT COALESCE(SUM(CASE WHEN cr.isLike = TRUE THEN 1 ELSE 0 END), 0) FROM c.reactions cr WHERE cr.comment = c), " +
            "(SELECT COALESCE(SUM(CASE WHEN cr.isLike = FALSE THEN 1 ELSE 0 END), 0) FROM c.reactions cr WHERE cr.comment = c), " +
            "c.createdAt, c.updatedAt, c.user, c.post.id) " +
            "FROM Comment c " +
            "WHERE c.id = :id AND c.isDeleted = FALSE")
    CommentDetailsQueryResult findCommentById(@Param("id") UUID id);

    @Transactional
    @Modifying
    @Query("UPDATE Comment c SET c.isDeleted = TRUE WHERE c.id = :id")
    void setDeleted(@Param("id") UUID id);

    @Transactional
    @Modifying
    @Query("UPDATE Comment c SET c.body = :body WHERE c.id = :id")
    void editComment(@Param("id") UUID id, @Param("body") String body);
}