package com.threadjava.users;

import com.threadjava.image.model.Image;
import com.threadjava.users.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;
import java.util.UUID;

public interface UsersRepository extends JpaRepository<User, UUID> {
    Optional<User> findByEmail(String email);

    @Transactional
    @Modifying
    @Query("UPDATE User u SET u.status = :status WHERE u.id = :userId")
    void editStatus(@Param("userId") UUID userId, @Param("status") String status);

    @Transactional
    @Modifying
    @Query("UPDATE User u SET u.avatar = :image WHERE u.id = :userId")
    void setImage(UUID userId, Image image);

    @Transactional
    @Modifying
    @Query("UPDATE User u SET u.email = :email WHERE u.id = :userId")
    void editEmail(@Param("userId") UUID userId, @Param("email") String email);
}