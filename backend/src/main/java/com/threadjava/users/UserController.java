package com.threadjava.users;

import com.threadjava.users.dto.UserDetailsDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;

import static com.threadjava.auth.TokenService.getUserId;

@RestController
@RequestMapping("/api/user")
public class UserController {
    @Autowired
    private UsersService userDetailsService;
    @Autowired
    private SimpMessagingTemplate template;

    @GetMapping
    public UserDetailsDto getUser() {
        return userDetailsService.getUserById(getUserId());
    }

    @PutMapping("/editStatus")
    public String editStatus(@RequestParam String status) {
        template.convertAndSendToUser(
                getUserId().toString(),
                "/statusChanged",
                "Your status has been changed."
        );
        return userDetailsService.editStatus(getUserId(), status);
    }

    @PutMapping("/uploadAvatar")
    public UUID uploadAvatar(@RequestParam UUID imageId) {
        userDetailsService.uploadAvatar(getUserId(), imageId);
        template.convertAndSendToUser(
                getUserId().toString(),
                "/avatarChanged",
                "Your avatar has been changed."
        );
        return imageId;
    }

    @PutMapping("/editEmail")
    public String editEmail(@RequestParam String email) {
        template.convertAndSendToUser(
                getUserId().toString(),
                "/emailChanged",
                "Your email has been changed."
        );
        return userDetailsService.editEmail(getUserId(), email);
    }
}
