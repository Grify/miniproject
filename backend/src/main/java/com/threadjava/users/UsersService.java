package com.threadjava.users;

import com.threadjava.auth.model.AuthUser;
import com.threadjava.image.ImageRepository;
import com.threadjava.image.model.Image;
import com.threadjava.users.dto.UserDetailsDto;
import com.threadjava.users.dto.UserShortDto;
import com.threadjava.users.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import java.util.UUID;

@Service
public class UsersService implements UserDetailsService {
    @Autowired
    private UsersRepository usersRepository;
    @Autowired
    private ImageRepository imageRepository;

    @Override
    public AuthUser loadUserByUsername(String email) throws UsernameNotFoundException {
        return usersRepository
                .findByEmail(email)
                .map(user -> new AuthUser(user.getId(), user.getEmail(), user.getPassword()))
                .orElseThrow(() -> new UsernameNotFoundException(email));
    }

    public UserDetailsDto getUserById(UUID id) {
        return usersRepository
                .findById(id)
                .map(user -> UserMapper.MAPPER.userToUserDetailsDto(user))
                .orElseThrow(() -> new UsernameNotFoundException("No user found with username"));
    }

    public String editStatus(UUID userId, String status) {
        if (status.length() > 70) {
            return "{ \"response\": \"Status is too long, max length is 70 chars.\"}";
        }
        usersRepository.editStatus(userId, status);
        return "{ \"response\": \"Success.\"}";
    }

    public void uploadAvatar(UUID userId, UUID imageId) {
        Image image = imageRepository.findById(imageId).orElse(null);
        usersRepository.setImage(userId, image);
    }

    public String editEmail(UUID userId, String email) {
        String regex = "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\\.[a-zA-Z0-9-]+)*$";
        if (!email.matches(regex)) {
            return "{ \"response\": \"Your email is invalid.\"}";
        }
        usersRepository.editEmail(userId, email);
        return "{ \"response\": \"Success.\"}";
    }

    public void save(User user) {
        usersRepository.save(user);
    }
}