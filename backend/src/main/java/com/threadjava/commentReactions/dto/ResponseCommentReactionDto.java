package com.threadjava.commentReactions.dto;

import lombok.Builder;
import lombok.Data;
import java.util.UUID;

@Data
@Builder
public class ResponseCommentReactionDto {
    private UUID id;
    private UUID commentId;
    private Boolean isLike;
    private Boolean isDislike;
    private Boolean wasChanged;
    private UUID userId;
    private UUID authorId;
}
