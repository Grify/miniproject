package com.threadjava.commentReactions;

import com.threadjava.comment.CommentService;
import com.threadjava.commentReactions.dto.ReceivedCommentReactionDto;
import com.threadjava.commentReactions.dto.ResponseCommentReactionDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class CommentReactionService {
    @Autowired
    private CommentReactionRepository commentReactionRepository;

    @Autowired
    private CommentService commentService;

    public Optional<ResponseCommentReactionDto> setReaction(ReceivedCommentReactionDto commentReactionDto) {

        var reaction = commentReactionRepository.getCommentReaction(commentReactionDto.getUserId(), commentReactionDto.getCommentId());

        if (reaction.isPresent()) {
            var react = reaction.get();
            if (react.getIsLike() == commentReactionDto.getIsLike() ||
                    react.getIsDislike() == commentReactionDto.getIsDislike()) {
                commentReactionRepository.deleteById(react.getId());
                return Optional.empty();
            } else {
                react.setIsLike(commentReactionDto.getIsLike());
                react.setIsDislike(commentReactionDto.getIsDislike());
                var result = commentReactionRepository.save(react);
                var comment = commentService.getCommentById(result.getComment().getId());
                return Optional.of(
                        ResponseCommentReactionDto
                                .builder()
                                .id(result.getId())
                                .isLike(result.getIsLike())
                                .isDislike(result.getIsDislike())
                                .wasChanged(true)
                                .userId(result.getUser().getId())
                                .commentId(comment.getId())
                                .authorId(comment.getUser().getId())
                                .build()
                );
            }
        } else {
            var commentReaction = CommentReactionMapper.MAPPER.dtoToCommentReaction(commentReactionDto);
            var result = commentReactionRepository.save(commentReaction);
            var comment = commentService.getCommentById(result.getComment().getId());
            return Optional.of(
                    ResponseCommentReactionDto
                            .builder()
                            .id(result.getId())
                            .isLike(result.getIsLike())
                            .isDislike(result.getIsDislike())
                            .wasChanged(false)
                            .userId(result.getUser().getId())
                            .commentId(comment.getId())
                            .authorId(comment.getUser().getId())
                            .build()
            );
        }
    }
}
