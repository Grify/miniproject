package com.threadjava.post.dto;

import lombok.Data;

import java.util.Date;
import java.util.UUID;

@Data
public class PostCommentDto {
    private UUID id;
    private String body;
    public long likeCount;
    public long dislikeCount;
    private PostUserDto user;
    private Date createdAt;
    private Date updatedAt;
    private UUID postId;
}
