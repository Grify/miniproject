package com.threadjava.post;

import com.threadjava.image.model.Image;
import com.threadjava.post.dto.PostDetailsQueryResult;
import com.threadjava.post.model.Post;
import com.threadjava.post.dto.PostListQueryResult;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface PostsRepository extends JpaRepository<Post, UUID> {

    @Query("SELECT new com.threadjava.post.dto.PostListQueryResult(p.id, p.body, " +
            "(SELECT COALESCE(SUM(CASE WHEN pr.isLike = TRUE THEN 1 ELSE 0 END), 0) FROM p.reactions pr WHERE pr.post = p), " +
            "(SELECT COALESCE(SUM(CASE WHEN pr.isLike = FALSE THEN 1 ELSE 0 END), 0) FROM p.reactions pr WHERE pr.post = p), " +
            "(SELECT COUNT(*) FROM p.comments pc WHERE pc.isDeleted = FALSE), " +
            "p.createdAt, i, p.user) " +
            "FROM Post p " +
            "LEFT JOIN p.image i " +
            "WHERE ((((:onlyOwnPosts = TRUE AND p.user.id = :userId) OR " +
            "(:onlyLiked = TRUE AND (SELECT COALESCE(SUM(CASE WHEN pr.isLike = TRUE AND pr.user = :userId THEN 1 ELSE 0 END), 0) FROM p.reactions pr WHERE pr.post = p) > 0) OR " +
            "(:onlyOthers = TRUE AND p.user.id <> :userId))" +
            "OR cast(:userId AS string) = NULL) AND p.isDeleted = FALSE)" +
            "ORDER BY p.createdAt DESC" )
    List<PostListQueryResult> findAllPosts(@Param("userId") UUID userId, @Param("onlyOwnPosts") Boolean onlyOwnPosts, @Param("onlyLiked") Boolean onlyLiked, @Param("onlyOthers") Boolean onlyOthers, Pageable pageable);

    @Query("SELECT new com.threadjava.post.dto.PostDetailsQueryResult(p.id, p.body, " +
            "(SELECT COALESCE(SUM(CASE WHEN pr.isLike = TRUE THEN 1 ELSE 0 END), 0) FROM p.reactions pr WHERE pr.post = p), " +
            "(SELECT COALESCE(SUM(CASE WHEN pr.isLike = FALSE THEN 1 ELSE 0 END), 0) FROM p.reactions pr WHERE pr.post = p), " +
            "(SELECT COUNT(*) FROM p.comments pc WHERE pc.isDeleted = FALSE), " +
            "p.createdAt, p.updatedAt, i, p.user) " +
            "FROM Post p " +
            "LEFT JOIN p.image i " +
            "WHERE p.id = :id")
    Optional<PostDetailsQueryResult> findPostById(@Param("id") UUID id);

    @Transactional
    @Modifying
    @Query("UPDATE Post p SET p.isDeleted = TRUE WHERE p.id = :id")
    void setDeleted(@Param("id") UUID id);

    @Transactional
    @Modifying
    @Query("UPDATE Post p SET p.body = :body, p.image = :image WHERE p.id = :id")
    void editPost(@Param("id") UUID id, @Param("body") String body, @Param("image") Image image);
}