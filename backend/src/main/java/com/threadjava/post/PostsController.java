package com.threadjava.post;


import com.threadjava.post.dto.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.web.bind.annotation.*;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static com.threadjava.auth.TokenService.getUserId;

@RestController
@RequestMapping("/api/posts")
public class PostsController {
    @Autowired
    private PostsService postsService;
    @Autowired
    private SimpMessagingTemplate template;

    @GetMapping
    public List<PostListDto> get(@RequestParam(defaultValue="0") Integer from,
                                 @RequestParam(defaultValue="10") Integer count,
                                 @RequestParam(required = false) UUID userId,
                                 @RequestParam(defaultValue="false") Boolean onlyOwnPosts,
                                 @RequestParam(defaultValue="false") Boolean onlyLiked,
                                 @RequestParam(defaultValue="false") Boolean onlyOthers){
        return postsService.getAllPosts(from, count, userId, onlyOwnPosts, onlyLiked, onlyOthers);
    }

    @GetMapping("/{id}")
    public PostDetailsDto get(@PathVariable UUID id) {
        return postsService.getPostById(id);
    }

    @PostMapping
    public PostCreationResponseDto post(@RequestBody PostCreationDto postDto) {
        postDto.setUserId(getUserId());
        var item = postsService.create(postDto);
        template.convertAndSend("/topic/new_post", item);
        template.convertAndSendToUser(
                getUserId().toString(),
                "/postAdded",
                "Your post was created!"
        );
        return item;
    }

    @DeleteMapping
    public UUID delete(@RequestParam UUID postId) {
        postsService.deletePostById(postId);
        template.convertAndSendToUser(
                getUserId().toString(),
                "/postDeleted",
                "Your post has been deleted."
        );
        return postId;
    }

    @PutMapping
    public PostEditionDto put(@RequestBody PostEditionDto postDto) {
        postsService.editPost(postDto);
        template.convertAndSendToUser(
                getUserId().toString(),
                "/postEdited",
                "Your post has been edited."
        );
        return postDto;
    }
}
